/* ******************************************************* */
/* I2C initialization code                                 */
/* ******************************************************* */

void I2cInit()
{
  Serial.print("initializing I2C (for accelo and magneto) ... ");
  Wire.begin();
  Serial.println("done");
}

