/* ******************************************************* */
/* I2C code for ADXL345 accelerometer                      */
/* ******************************************************* */

int       AccMeter = 0x53;
int       AccRaw[3];        // (Y axis, X axis, Z axis || int.X axis, int.Y axis, int.Z axis)
boolean   AccError;
int       accel_x;
int       accel_y;
int       accel_z;

void AccInit()
{
  Serial.print("initializing Accelo ... ");
  Wire.beginTransmission(AccMeter);
  Wire.write(0x2D);  // power register
  Wire.write(0x08);  // measurement mode
  Wire.endTransmission();
  delay(5);
  Wire.beginTransmission(AccMeter);
  Wire.write(0x31);  // Data format register
  Wire.write(0x08);  // set to full resolution
  Wire.endTransmission();
  delay(5);	
  // Because our main loop runs at 50Hz we adjust the output data rate to 50Hz (25Hz bandwidth)
  Wire.beginTransmission(AccMeter);
  Wire.write(0x2C);  // Rate
  Wire.write(0x09);  // set to 50Hz, normal operation
  Wire.endTransmission();
  delay(5);
  Serial.println("done");
}



// Reads x,y and z accelerometer registers
void AccRead()
{
  int i = 0;
  byte* AccPtr = (byte*)AccRaw;
  Wire.beginTransmission(AccMeter); 
  Wire.write(0x32);        //sends address to read from
  Wire.endTransmission();  //end transmission
  Wire.beginTransmission(AccMeter); //start transmission to device
  Wire.requestFrom(AccMeter, 6);    // request 6 bytes from device
  while(Wire.available())   // ((Wire.available())&&(i<6))
  { 
    *AccPtr = Wire.read();  // receive one byte
    AccPtr++; i++;
  }
  Wire.endTransmission();   //end transmission
  if (i != 6) AccError = true;
  else
  {
    AN[3] = AccRaw[0];
    AN[4] = AccRaw[1];
    AN[5] = AccRaw[2];
    accel_x = SENSOR_SIGN[3]*(AccRaw[0]-AN_OFFSET[3]);
    accel_y = SENSOR_SIGN[4]*(AccRaw[1]-AN_OFFSET[4]);
    accel_z = SENSOR_SIGN[5]*(AccRaw[2]-AN_OFFSET[5]);
  }
}
