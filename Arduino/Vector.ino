float VectDotProd(float a[3], float b[3])
{
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]; 
}

void VectCrsProd(float dst[3], float a[3], float b[3])
{
  dst[0]= (a[1] * b[2]) - (a[2] * b[1]);
  dst[1]= (a[2] * b[0]) - (a[0] * b[2]);
  dst[2]= (a[0] * b[1]) - (a[1] * b[0]);
}

void VectScale(float dst[3], float a[3], float fact)
{
  for(int i=0; i<3; i++)
   dst[i] = a[i] * fact; 
}

void VectAdd(float dst[3], float a[3], float b[3])
{
  for(int i=0; i<3; i++)
     dst[i] = a[i] + b[i];
}

