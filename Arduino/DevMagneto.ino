/* ******************************************************* */
/* I2C code for HMC5843 magnetometer                       */
/* ******************************************************* */

int       MagMeter = 0x1E;  //0x3C //0x3D;  //(0x42>>1);
int       MagRaw[3];        // (Y axis, X axis, Z axis || int.X axis, int.Y axis, int.Z axis)
boolean   MagError;
int       magnetom_x;
int       magnetom_y;
int       magnetom_z;


void MagInit()
{
  Wire.beginTransmission(MagMeter);
  Wire.write(0x02); 
  Wire.write(0x00);       // Set continuos mode (default to 10Hz)
  Wire.endTransmission(); //end transmission
}



void MagRead()
{
  int i = 0;
  byte* MagPtr = (byte*)MagRaw;
  Wire.beginTransmission(MagMeter); 
  Wire.write(0x03);        //sends address to read from
  Wire.endTransmission(); //end transmission
  //Wire.beginTransmission(MagMeter); 
  Wire.requestFrom(MagMeter, 6);    // request 6 bytes from device
  // MSB byte first, then LSB, X,Y,Z
  while(Wire.available())   // ((Wire.available())&&(i<6))
  {
    MagPtr[1] = Wire.read(); i++; // receive msb
    if(Wire.available())
    {
      *MagPtr = Wire.read();  // recieve lsb
      i++;
    }
    MagPtr+=2;
  }
  Wire.endTransmission(); //end transmission
  if(i != 6) MagError = true;
  else
  {
    magnetom_x = SENSOR_SIGN[6] * MagRaw[1];    // X axis (internal sensor y axis)
    magnetom_y = SENSOR_SIGN[7] * MagRaw[0];    // Y axis (internal sensor x axis)
    magnetom_z = SENSOR_SIGN[8] * MagRaw[2];    // Z axis
  }
}

