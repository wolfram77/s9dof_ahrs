// Matrix Operations

// Multiply two 3x3 matrices
// This function developed by Jordi can be easily
// adapted to multiple n*n matrices. (Pero me da flojera!). 
// simplified by Subhajit Sahu (github/wolfram77)
void MatMul(float a[3][3], float b[3][3], float dst[3][3])
{
  float sum;
  int row, col, elem;
  for(row=0; row<3; row++)
  {
    for(col=0; col<3; col++)
    {
      sum = 0.0f;
      for(elem=0; elem<3; elem++)
        sum += a[row][elem] * b[elem][col];
      dst[row][col] = sum;
    }
  }
}


