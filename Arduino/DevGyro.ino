/* ******************************************************* */
/* ADC code for LY530AL (1-axis), LPR530AL (2-axis) gyros  */
/* ******************************************************* */

//ADC variables
volatile uint8_t  AdcSel    = 0;
volatile uint8_t  AdcRef;
volatile uint16_t AdcBuff[8];
volatile uint8_t  AdcRdCount[8];


// We are using an oversampling and averaging method to increase the ADC resolution
// Now we store the ADC readings in float format
void Read_adc_raw(void)
{
  int i;
  uint16_t temp1;
  uint8_t temp2;
  
  // ADC readings...
  for (i=0;i<3;i++)
    {
      do{
        temp1= AdcBuff[sensors[i]];             // sensors[] maps sensors to correct order 
        temp2= AdcRdCount[sensors[i]];
        } while(temp1 != AdcBuff[sensors[i]]);  // Check if there was an ADC interrupt during readings...
      
      if (temp2>0) AN[i] = (float)temp1/(float)temp2;     // Check for divide by zero 
            
    }
  // Initialization for the next readings...
  for (int i=0;i<3;i++){
    do{
      AdcBuff[i]=0;
      AdcRdCount[i]=0;
      } while(AdcBuff[i]!=0); // Check if there was an ADC interrupt during initialization...
  }
}



float read_adc(int select)
{
  if (SENSOR_SIGN[select]<0)
    return(AN_OFFSET[select]-AN[select]);
  else
    return(AN[select]-AN_OFFSET[select]); 
}



void AdcInit()
{
  Serial.print("initializing adc(for gyro) ... ");
  // set analog refernce
  AdcRef = DEFAULT;
  // activate adc interrupts
  ADCSRA |= (1 << ADIE)|(1 << ADEN);
  ADCSRA |= (1 << ADSC);
  Serial.println("done");
}



//ADC interrupt vector, this piece of code
//is executed everytime a convertion is done. 
ISR(ADC_vect)
{
  volatile uint8_t adc_lsb = ADCL, adc_msb = ADCH;
  if(AdcRdCount[AdcSel] < 63)
  {
    // accumulate analog values, since they represent
    // a change, and not an instantaneous value
    AdcBuff[AdcSel] += (adc_msb << 8) | adc_lsb;
    AdcRdCount[AdcSel]++;
  }
  AdcSel++; AdcSel &= 0x03;   //if(AdcSel >=4) AdcSel=0;
  ADMUX = (AdcRef << 6) | AdcSel;
  // start the conversion
  ADCSRA |= (1<<ADSC);
}

