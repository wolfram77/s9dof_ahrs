// New function: printdata(void)
// 
// 
// 
// 

// properties
char     Cmd[16];
boolean  isOutputEnabled;
boolean  isBinaryEnabled;
// storing raw sensor data
int  SnsrRawPtr;
float  SnsrRaw[36];
float  SnsrSum[9];
// storing calculated data
float  SnsrNew[9];
float  SnsrOld[9];
// constants
float  SnsrMulFact[] =
{
  1.0f,      1.0f,      1.0f,
  0.003125f, 0.003125f, 0.003125f,
  0.00125f,  0.00125f,  0.00125f
};
float  SnsrAddFact[] =
{
  0.0f,    0.0f,    0.0f,
  0.0f,    0.0f,    0.0f,
  0.0f,    0.0f,    0.0f
};
float  SnsrDelMin[] =
{
  0.0f,    0.0f,    0.0f,
  0.0f,    0.0f,    0.0f,
  0.0f,    0.0f,    0.0f
};


// Function:
// RunCommand()
//
// Runs commands sent by Processing code (ex.- to sync) 
// 
// Parameters:
// none
// 
// Returns:
// nothing
// 
void RunCommand()
{
  if(Serial.available() == 0) return;
  Serial.readBytesUntil('\0', Cmd, 16);
  if(strcmp(Cmd, "sync") == 0) Serial.write((byte*)"sync", 4);
  else if(strcmp(Cmd, "bin") == 0) isBinaryEnabled = true;
  else if(strcmp(Cmd, "text") == 0) isBinaryEnabled = false;
  else if(strcmp(Cmd, "start") == 0) isOutputEnabled = true;
  else if(strcmp(Cmd, "stop") == 0) isOutputEnabled  = false;
}


void GetSnsrRaw()
{
  int i;
  float* dat = SnsrRaw + SnsrRawPtr;
  // update the sum before removal
  for(i=0; i<9; i++)
    SnsrSum[i] -= dat[i];
  // get the new data
  dat[0] = (float)ToDeg(roll);
  dat[1] = (float)ToDeg(pitch);
  dat[2] = (float)ToDeg(yaw);
  dat[3] = (float)AccRaw[0];
  dat[4] = (float)AccRaw[1];
  dat[5] = (float)AccRaw[2];
  dat[6] = (float)magnetom_x;
  dat[7] = (float)magnetom_y;
  dat[8] = (float)magnetom_z;
  // update the sum after addition
  for(i=0; i<9; i++)
    SnsrSum[i] += dat[i];
}


void GetSnsrNew()
{
  int i;
  float diff;
  // save old sensor readings
  memcpy(SnsrOld, SnsrNew, 36);
  // perform requisite scaling
  for(i=0; i<9; i++)
    SnsrNew[i] = SnsrSum[i] * SnsrMulFact[i] + SnsrAddFact[i];
  // remove drift in readings
  for(i=0; i<9; i++)
  {
    diff = SnsrNew[i] - SnsrOld[i];
    diff = (diff < 0)? -diff : diff;
    if(diff < SnsrDelMin[i]) SnsrNew[i] = SnsrOld[i];
  }
}


// Writes a 36 byte packet with header "ABCD"
// for allowing the processing code to sync
void SendPkt(void)
{
  int i;
  if(!isOutputEnabled) return;
  if(isBinaryEnabled)
  {
    Serial.write((byte*)"ABCD", 4);
    Serial.write((byte*)SnsrNew, 36);
  }
  else
  {
    Serial.print("GYR: ");
    for(i=0; i<3; i++)
    {
      Serial.print(SnsrNew[i]);
      Serial.print(" | ");
    }
    Serial.print(" ACC: ");
    for(i=3; i<6; i++)
    {
      Serial.print(SnsrNew[i]);
      Serial.print(" | ");
    }
    Serial.print(" MAG: ");
    for(i=6; i<9; i++)
    {
      Serial.print(SnsrNew[i]);
      Serial.print(" | ");
    }
    Serial.println("");
  }
}


void printdata()
{
  RunCommand();
  GetSnsrRaw();
  GetSnsrNew();
  SendPkt();
}



/*
// Old function: printdata(void)
// 
// Released under Creative Commons License 
// Code by Doug Weibel and Jose Julio
// Based on ArduIMU v1.5 by Jordi Munoz and William Premerlani, Jose Julio and Doug Weibel
//
void printdata_org(void)
{    
      //Serial.print("!");
      
//      #if PRINT_EULER == 1
      //Serial.print("ANG:");
//      Serial.print(ToDeg(roll));
//      Serial.print(",");
//      Serial.print(ToDeg(pitch));
//      Serial.print(",");
//      Serial.print(ToDeg(yaw));
//      Serial.print(",");
//      Serial.print( Accel_Scale(accel_x) );
//      Serial.print (",");
//      Serial.print( Accel_Scale(accel_y) );
//      Serial.print (",");
//      Serial.print( Accel_Scale(accel_z) );
      //Serial.print(",");
      //Serial.print(ToDeg(MAG_Heading));
//      #endif

      #if PRINT_EULER == 1
      //Serial.print("ANG:");
      Serial.print(ToDeg(roll));
      Serial.print(",");
      Serial.print(ToDeg(pitch));
      Serial.print(",");
      Serial.print(ToDeg(yaw));
      #endif      
      #if PRINT_ANALOGS==1
      Serial.print(",AN:");
      Serial.print(AN[sensors[0]]);  //(int)read_adc(0)
      Serial.print(",");
      Serial.print(AN[sensors[1]]);
      Serial.print(",");
      Serial.print(AN[sensors[2]]);  
      Serial.print(",");
      Serial.print(ACC[0]);
      Serial.print (",");
      Serial.print(ACC[1]);
      Serial.print (",");
      Serial.print(ACC[2]);
      Serial.print(",");
      Serial.print(magnetom_x);
      Serial.print (",");
      Serial.print(magnetom_y);
      Serial.print (",");
      Serial.print(magnetom_z);      
      #endif
      #if PRINT_DCM == 1
      Serial.print (",DCM:");
      Serial.print(convert_to_dec(DCM_Matrix[0][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[0][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[0][2]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[1][2]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][0]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][1]));
      Serial.print (",");
      Serial.print(convert_to_dec(DCM_Matrix[2][2]));
      #endif
      Serial.println();    
      
}
*/

long convert_to_dec(float x)
{
  return x*10000000;
}

